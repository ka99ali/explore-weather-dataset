import streamlit as st
import numpy as np
import plotly.graph_objects as go
import pandas as pd

df = pd.read_csv("Weather.csv")

st.header('Explore Weather DataSet')

st.write('### head')
st.write(df.head())

st.write('### shape')
st.write(df.shape)

st.write('### index')
st.write(df.index)

st.write('### columns')
st.write("Answer: " + str(df.columns.tolist()))

st.write('### unique values')
st.write("Answer: " + str(df['Weather'].unique().tolist()))

st.write('### values Counts')
st.write(df['Weather'].value_counts())

st.markdown("<hr>", unsafe_allow_html=True)
st.header('Questions')

st.subheader("Q 1. Find all the unique 'Wind Spead' values in the data.")
st.write(
    "Answer: " + str(df['Wind Speed_km/h'].unique().tolist())   # unique values 
    + " - count of unique values: " + str(df['Wind Speed_km/h'].nunique())       # Count unique values 
    )

st.subheader("Q 2. Find the number of times when the Weather is exactly 'Snow'.")
st.write("#### Answer 1:")
st.write(df[df.Weather == 'Snow'].head())
st.write("#### Answer 2:")
st.write(df.groupby('Weather').get_group('Snow').head())
st.write("#### Answer 3:")
st.write(df[df.Weather.str.contains('Sno')].head())

st.subheader("Q 3. Find the number of times when the 'Wind Speed was exactly 0 Km/h'.")
st.write(df[df['Wind Speed_km/h'] == 0 ])

st.subheader("Q 4. Find out all the null values in The Data.")
st.write(df.isnull().sum())

st.subheader("Q 5. What is mean of Visibility.")
st.write("mean of Visibility: " + str(df.Visibility_km.mean()))

st.subheader("Q 6. What is Standard Deviation of 'Pressure' in this Data.")
st.write("Statndard Deviation of Pressure: " + str(df.Press_kPa.std()))


st.subheader("Q 7. What is Variance of 'Relative Humidity' in this Data.")
st.write("Variance of Relative Humidity: " + str(df['Rel Hum_%'].var()))

st.subheader("Q 8. Find all instances When 'Wind Spead is above 24' and 'Visibility is 25'.")
st.write(df[ (df['Wind Speed_km/h'] > 24) & ( df.Visibility_km == 25) ])

st.subheader("Q 9. What is mean value of each col against each 'Weather'.")
st.write(df.groupby('Weather').mean())

st.subheader("Q 10. What is max & min value of each col against each 'Weather'.")
st.write("#### Max value")
st.write(df.groupby('Weather').max())
st.write("#### Min value")
st.write(df.groupby('Weather').min())

st.subheader("Q 11. Find all instances When 'Weather is Clear' or 'Visibility is above 40'.")
st.write(df[ (df.Weather == 'Clear') | ( df.Visibility_km > 40) ])


st.subheader("Q 12. Find all instances When 'Weather is Clear' and  'Relative Humidity is Greater than 50'  or 'Visibility is above 40'.")
st.write(
    df[ 
        ((df.Weather == 'Clear') & (df['Rel Hum_%'] > 50)) 
        | 
        ( df.Visibility_km > 40) ])